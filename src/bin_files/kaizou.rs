use crate::dbs::{a_abilities::AutoAbility, items::Item};

use super::DataFileHeader;

pub enum KaizouType {
    None = 0x0,
    Weapon = 0x1,
    Armor = 0x2,
    Both = 0x3,
}

pub struct KaizouBlock<'a> {
    pub header: DataFileHeader,
    pub kaizou_type: KaizouType,
    pub ability: &'a AutoAbility,
    pub item: &'a Item,
    pub item_count: u16,
}

pub struct KaizouFile<'a> {
    pub blocks: Vec<KaizouBlock<'a>>,
}
