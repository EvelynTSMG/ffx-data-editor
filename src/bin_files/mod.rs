pub mod kaizou;

pub trait DataFile {
    fn save_bin(&self, filepath: &str) -> bool;
    fn load_bin(filepath: &str) -> Self;
}

pub struct DataFileHeader {
    pub unk1: u32,
    pub unk2: u32,
    pub unk3: u16,
    pub max_idx: u16,
    pub block_size: u16,
    pub data_len: u16,
    pub data_start_offset: u32,
}
