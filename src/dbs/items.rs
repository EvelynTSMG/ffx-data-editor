use super::{
    command::{self, Command},
    misc::Elements,
};

pub struct Item {
    id: u16,
    ability: Command,
}
